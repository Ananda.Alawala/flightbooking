$(document).ready(function () {

    $("#search-form").submit(function (event) {

        //stop submit the form, we will post it manually.
        event.preventDefault();

        //fire_ajax_submit();
        var search = {}
            search["sourceCity"] = $("#SourceCity").val();
            search["destinationCity"] = $("#DestinationCity").val();
            search["numberOfPassengers"] = $("#NumberOfPassengers").val()||1;
            search["departureDate"] = $("#departureDate").val();
            search["travelClass"] = $("#travelClass").val();

            $("#btn-search").prop("disabled", true);

            $.ajax({
                type: "POST",
                contentType: "application/json",
                url: "/api/search",
                data: JSON.stringify(search),
                dataType: 'json',
                cache: false,
                timeout: 600000,
                success: function (data) {

                    var json = "<h4>Ajax Response</h4><pre>"
                        + JSON.stringify(data, null, 4) + "</pre>";
                    $('#feedback').html(json);

                    console.log("SUCCESS : ", data);
                    $("#btn-search").prop("disabled", false);

                     /*var tr;
                                  // $('#result').hidden=false;
                                   $('#result').find("tbody").empty();
                              for (var i = 0; i < data.result.length; i++) {
                                  var vals = data.result[i].split("<br>");
                                  tr = $("<tr/>");
                                  tr.append("<td>" + vals[0].split(':')[1] + "</td>");
                                  tr.append("<td>" + vals[1].split(':')[1] + "</td>");
                                  tr.append("<td>" + vals[2].split(':')[1] + "</td>");
                                  tr.append("<td>" + vals[3].split(':')[1]  + "</td>");
                                  tr.append("<td>" + vals[4].split(':')[1] + "</td>");
                                  tr.append("<td>" + vals[5].split(':')[1] + "</td>");
                                  tr.append("<td>" + vals[6].split(':')[1] + "</td>");
                                 // tr.append("<td>" + vals[7].split(':')[1] + "</td>");
                                  $("#result").find("tbody").append(tr);
                              }*/

                },
                error: function (e) {

                    var json = "<h4>Ajax Response</h4><pre>"
                        + e.responseText + "</pre>";
                    $('#feedback').html(json);

                    console.log("ERROR : ", e);
                    $("#btn-search").prop("disabled", false);

                }



    });

});

});