package org.FlightBooking.Model;

import java.util.List;

public class Flight {

    private int flightId;
    private String model;
    private String carrier;
    private int noOfSeats;

    public Flight(int flightId,String model,String carrier) {
        this.flightId = flightId;
        this.model = model;
        this.carrier = carrier;
    }

    public int getFlightId() {
        return flightId;
    }

    public void setFlightId(int flightId) {
        this.flightId = flightId;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getCarrier() {
        return carrier;
    }

    public void setCarrier(String carrier) {
        this.carrier = carrier;
    }

    public int getNoOfSeats() {
        return noOfSeats;
    }

    public void setNoOfSeats(int noOfSeats) {
        this.noOfSeats = noOfSeats;
    }
}