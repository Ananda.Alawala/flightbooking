package org.FlightBooking.Model;

public class TravelClass {

        private int classId;
        private String name;
        private int noOfSeats;
        private Double basePrice;
        private int availableSeats;

    public TravelClass(int classId,String name,int noOfSeats, Double basePrice, int availableSeats) {
        this.classId = classId;
        this.name = name;
        this.noOfSeats = noOfSeats;
        this.basePrice = basePrice;
        this.availableSeats = availableSeats;
    }

    public int getClassId() {
        return classId;
    }

    public void setClassId(int classId) {
        this.classId = classId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getNoOfSeats() {
        return noOfSeats;
    }

    public void setNoOfSeats(int noOfSeats) {
        this.noOfSeats = noOfSeats;
    }

    public Double getBasePrice() {
        return basePrice;
    }

    public void setBasePrice(Double basePrice) {
        this.basePrice = basePrice;
    }

    public int getAvailableSeats() {
        return availableSeats;
    }

    public void setAvailableSeats(int availableSeats) {
        this.availableSeats = availableSeats;
    }
}



