package org.FlightBooking.Model;

import java.util.List;

public class FlightResponseBody {
    String msg;
    List<TripDetails> result;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<TripDetails> getResult() {
        return result;
    }

    public void setResult(List<TripDetails> result) {
        this.result = result;
    }
}
