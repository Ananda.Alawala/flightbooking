package org.FlightBooking.Model;

import javax.validation.constraints.NotBlank;
import java.time.LocalDate;
import java.util.Date;

public class SearchCriteria {
    @NotBlank(message = "Source cannot be Blank! Please fill to Proceed.")
    private String sourceCity;

    @NotBlank(message = "Destination cannot be Blank! Please fill to Proceed.")
    private String destinationCity;

    private int numberOfPassengers;

    private LocalDate departureDate;

    private String travelClass;

    public String getSourceCity() {
        return sourceCity;
    }

    public void setSourceCity(String sourceCity) {
        this.sourceCity = sourceCity;
    }

    public String getDestinationCity() {
        return destinationCity;
    }

    public void setDestinationCity(String destinationCity) {
        this.destinationCity = destinationCity;
    }

    public int getNumberOfPassengers() {
        return numberOfPassengers;
    }

    public void setNumberOfPassengers(int numberOfPassengers) {
        this.numberOfPassengers = numberOfPassengers;
    }

    public LocalDate getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(LocalDate departureDate) {
        this.departureDate = departureDate;
    }

    public String getTravelClass() {
        return travelClass;
    }

    public void setTravelClass(String travelClass) {
        this.travelClass = travelClass;
    }
}
