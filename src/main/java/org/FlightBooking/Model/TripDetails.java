package org.FlightBooking.Model;

import java.time.LocalDate;
import java.util.Date;

public class TripDetails {
    private String source;
    private String destination;
    private Flight flight;
    private TravelClass travelClass;
    private int tripId;
    private LocalDate departureDate;
    private Double bookingAmount;


    public TripDetails(int tripId, String source, String destination, Flight flight, LocalDate departureDate, TravelClass travelClass) {
        this.tripId = tripId;
        this.departureDate = departureDate;
        this.source = source;
        this.destination = destination;
        this.flight = flight;
        this.travelClass = travelClass;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public Flight getFlight() {
        return flight;
    }

    public void setFlight(Flight flight) {
        this.flight = flight;
    }

    public TravelClass getTravelClass() {
        return travelClass;
    }

    public void setTravelClass(TravelClass travelClass) {
        this.travelClass = travelClass;
    }

    public int getTripId() {
        return tripId;
    }

    public void setTripId(int tripId) {
        this.tripId = tripId;
    }

    public LocalDate getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(LocalDate departureDate) {
        this.departureDate = departureDate;
    }


    public Double getBookingAmount() {
        return bookingAmount;
    }

    public void setBookingAmount(Double bookingAmount) {
        this.bookingAmount = bookingAmount;
    }

}


