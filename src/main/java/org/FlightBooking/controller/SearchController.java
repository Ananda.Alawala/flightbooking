package org.FlightBooking.controller;

import org.FlightBooking.Model.FlightResponseBody;
import org.FlightBooking.Model.SearchCriteria;
import org.FlightBooking.Model.TripDetails;
import org.FlightBooking.Services.FareCalculateService;
import org.FlightBooking.Services.FlightSearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@RestController
public class SearchController {

    @Autowired
    public FlightSearchService flightSearchService;

    @Autowired
    public FareCalculateService fareCalculateService;


    public void setFlightSearchService(FlightSearchService flightSearchService){
        this.flightSearchService = flightSearchService;
    }

    public void setFareCalculateService(FareCalculateService fareCalculateService){
        this.fareCalculateService = fareCalculateService;
    }


    @PostMapping("/api/search")
    public ResponseEntity<?> getSearchResultViaAjax(@RequestBody SearchCriteria search,Errors errors) throws ParseException {

        FlightResponseBody result = new FlightResponseBody();


        if (errors.hasErrors()) {

            result.setMsg(errors.getAllErrors().stream()
                    .map(x -> x.getDefaultMessage())
                    .collect(Collectors.joining(",")));
            return ResponseEntity.badRequest().body(result);
        }
            List<TripDetails> tripDetails = flightSearchService.findBySourceDestinationOnADayInAClass(search.getSourceCity(),search.getDestinationCity(),search.getNumberOfPassengers(),search.getTravelClass(),search.getDepartureDate());
            if (tripDetails.isEmpty()) {
                result.setMsg("No Flights Found");
            } else {
                result.setMsg("Success");
            }
            List<TripDetails> tripDetailsWithBookingAmount = fareCalculateService.fareCalculateServiceForEachClass(tripDetails,search.getNumberOfPassengers());
            result.setResult(tripDetailsWithBookingAmount);
            return ResponseEntity.ok(result);

        }

        }


