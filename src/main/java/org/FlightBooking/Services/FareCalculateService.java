package org.FlightBooking.Services;

import org.FlightBooking.Model.TripDetails;
import org.FlightBooking.Repository.TripDetailsRepository;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.List;
import java.util.ListIterator;
import java.util.stream.Collectors;

@Service
public class FareCalculateService {
    private List<TripDetails> trips;

    private TripDetailsRepository repository;
    SimpleDateFormat obgD = new SimpleDateFormat("yyyy-mm-dd");

    public FareCalculateService(TripDetailsRepository repository) {
        this.repository = repository;
    }

    public List<TripDetails> fareCalculateServiceForEachClass(List<TripDetails> result,int noOfPassengers){

        Double BookingAmount;
        ListIterator<TripDetails> iterator = result.listIterator();

        while (iterator.hasNext()) {
            TripDetails temp = iterator.next();
            if (temp.getTravelClass().getName().equalsIgnoreCase("Economy Class")) {
                temp = fareCalculatorForEconomyClass(temp,noOfPassengers);
            } else if (temp.getTravelClass().getName().equalsIgnoreCase("Business Class")) {
                temp = fareCalculatorForBusinessClass(temp,noOfPassengers);
            } else {
                temp = fareCalculatorForFirstClass(temp,noOfPassengers);
            }
        }
            return result;

    }

    public TripDetails fareCalculatorForEconomyClass(TripDetails tripDetails,int noOfPassengers){

        Double BookingAmount;
        if (tripDetails.getTravelClass().getAvailableSeats() > 0.6 * tripDetails.getTravelClass().getNoOfSeats()) {
            BookingAmount = tripDetails.getTravelClass().getBasePrice() * noOfPassengers;
            tripDetails.setBookingAmount(BookingAmount);
        } else if ((tripDetails.getTravelClass().getAvailableSeats() < 0.6 * tripDetails.getTravelClass().getNoOfSeats()) && (tripDetails.getTravelClass().getAvailableSeats() > 0.1 * tripDetails.getTravelClass().getNoOfSeats())) {
            BookingAmount = (tripDetails.getTravelClass().getBasePrice() + 0.3 * tripDetails.getTravelClass().getBasePrice()) * noOfPassengers;
            tripDetails.setBookingAmount(BookingAmount);
        } else if ((tripDetails.getTravelClass().getAvailableSeats() < 0.1 * tripDetails.getTravelClass().getNoOfSeats())) {
            BookingAmount = (tripDetails.getTravelClass().getBasePrice() + 0.6 * tripDetails.getTravelClass().getBasePrice()) * noOfPassengers;
            tripDetails.setBookingAmount(BookingAmount);
        }
        return tripDetails;
    }

    public TripDetails fareCalculatorForBusinessClass(TripDetails tripDetails, int noOfPassengers){
        Double BookingAmount;
        if ((tripDetails.getDepartureDate().getDayOfWeek().name().equalsIgnoreCase("Monday")) || (tripDetails.getDepartureDate().getDayOfWeek().name().equalsIgnoreCase("Friday") ||
                (tripDetails.getDepartureDate().getDayOfWeek().name().equalsIgnoreCase("Sunday")))) {
            BookingAmount = (tripDetails.getTravelClass().getBasePrice() + 0.4 * tripDetails.getTravelClass().getBasePrice()) * noOfPassengers;
            tripDetails.setBookingAmount(BookingAmount);
        }
        else{
            BookingAmount = tripDetails.getTravelClass().getBasePrice() * noOfPassengers;
            tripDetails.setBookingAmount(BookingAmount);
        }
        return tripDetails;
    }

    public TripDetails fareCalculatorForFirstClass(TripDetails tripDetails,int noOfPassengers){
        Double BookingAmount;
        if (tripDetails.getDepartureDate().compareTo(LocalDate.now()) > 9) {
            BookingAmount = (tripDetails.getTravelClass().getBasePrice() + 0.1 * tripDetails.getTravelClass().getBasePrice()) * noOfPassengers;
            tripDetails.setBookingAmount(BookingAmount);
        } else if (tripDetails.getDepartureDate().compareTo(LocalDate.now()) > 8) {
            BookingAmount = (tripDetails.getTravelClass().getBasePrice() + 0.2 * tripDetails.getTravelClass().getBasePrice()) * noOfPassengers;
            tripDetails.setBookingAmount(BookingAmount);
        } else if (tripDetails.getDepartureDate().compareTo(LocalDate.now()) > 7) {
            BookingAmount = (tripDetails.getTravelClass().getBasePrice() + 0.3 * tripDetails.getTravelClass().getBasePrice()) * noOfPassengers;
            tripDetails.setBookingAmount(BookingAmount);
        } else if (tripDetails.getDepartureDate().compareTo(LocalDate.now()) > 6) {
            BookingAmount = (tripDetails.getTravelClass().getBasePrice() + 0.4 * tripDetails.getTravelClass().getBasePrice()) * noOfPassengers;
            tripDetails.setBookingAmount(BookingAmount);
        } else if (tripDetails.getDepartureDate().compareTo(LocalDate.now()) > 5) {
            BookingAmount = (tripDetails.getTravelClass().getBasePrice() + 0.5 * tripDetails.getTravelClass().getBasePrice()) * noOfPassengers;
            tripDetails.setBookingAmount(BookingAmount);
        } else if (tripDetails.getDepartureDate().compareTo(LocalDate.now()) > 4) {
            BookingAmount = (tripDetails.getTravelClass().getBasePrice() + 0.6 * tripDetails.getTravelClass().getBasePrice()) * noOfPassengers;
            tripDetails.setBookingAmount(BookingAmount);
        } else if (tripDetails.getDepartureDate().compareTo(LocalDate.now()) > 3) {
            BookingAmount = (tripDetails.getTravelClass().getBasePrice() + 0.7 * tripDetails.getTravelClass().getBasePrice()) * noOfPassengers;
            tripDetails.setBookingAmount(BookingAmount);
        } else if (tripDetails.getDepartureDate().compareTo(LocalDate.now()) > 2) {
            BookingAmount = (tripDetails.getTravelClass().getBasePrice() + 0.8 * tripDetails.getTravelClass().getBasePrice()) * noOfPassengers;
            tripDetails.setBookingAmount(BookingAmount);
        } else if (tripDetails.getDepartureDate().compareTo(LocalDate.now()) > 1) {
            BookingAmount = (tripDetails.getTravelClass().getBasePrice() + 0.9 * tripDetails.getTravelClass().getBasePrice()) * noOfPassengers;
            tripDetails.setBookingAmount(BookingAmount);
        } else if (tripDetails.getDepartureDate().compareTo(LocalDate.now()) == 1) {
            BookingAmount = (tripDetails.getTravelClass().getBasePrice() + 1 * tripDetails.getTravelClass().getBasePrice()) * noOfPassengers;
            tripDetails.setBookingAmount(BookingAmount);
        }
        return tripDetails;
    }

}
