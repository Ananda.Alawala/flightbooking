package org.FlightBooking.Services;

import org.FlightBooking.Model.TripDetails;
import org.FlightBooking.Repository.TripDetailsRepository;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ListIterator;
import java.util.stream.Collectors;

@Service
public class FlightSearchService {
    private List<TripDetails> trips;

    private TripDetailsRepository repository;
    SimpleDateFormat obgD = new SimpleDateFormat("yyyy-mm-dd");

    public FlightSearchService(TripDetailsRepository repository) {
        this.repository = repository;
    }


    public List<TripDetails> findBySourceDestinationOnADayInAClass(String source, String destination, int noOfPassengers, String travelClass, LocalDate departureDate) {
        trips = repository.getTripRepository();
        if (source.isEmpty() || destination.isEmpty()) {
            return null;
        }
        if (noOfPassengers == 0) {
            noOfPassengers = 1;
        }
        int finalNoOfPassengers = noOfPassengers;

        List<TripDetails> result = new ArrayList();
        List<TripDetails> result1 = new ArrayList();
        if (departureDate == null) {
            result = trips.stream()
                    .filter(x -> ((x.getSource().equalsIgnoreCase(source)) &&
                            (x.getDestination().equalsIgnoreCase(destination))&&
                            (x.getTravelClass().getName().equalsIgnoreCase(travelClass))&&
                            (x.getTravelClass().getNoOfSeats() > finalNoOfPassengers)))
                    .collect(Collectors.toList());
        }
        else {
            result = trips.stream()
                    .filter(x -> ((x.getSource().equalsIgnoreCase(source)) &&
                            (x.getDestination().equalsIgnoreCase(destination)) &&
                            (x.getDepartureDate().compareTo(departureDate) == 0) &&
                            (x.getTravelClass().getName().equalsIgnoreCase(travelClass))&&
                            (x.getTravelClass().getNoOfSeats() > finalNoOfPassengers)))
                    .collect(Collectors.toList());
        }
        Period interval = null;
//        for (TripDetails r: result) {
//
//            interval = Period.between(LocalDate.now(),r.getDepartureDate());
//            System.out.println(LocalDate.now());
//            System.out.println(r.getDepartureDate());
//            System.out.println(interval.getDays());
//            if (((r.getTravelClass().getName().equalsIgnoreCase("Business Class"))&&
//                    (interval.getDays()<=28))||
//                    ((r.getTravelClass().getName().equalsIgnoreCase("First Class"))&&
//                            (interval.getDays())<=10)||
//                    (r.getTravelClass().getName().equalsIgnoreCase("Economy Class")))
//                System.out.println(r.getTravelClass().getName());
//        }


        Period finalInterval = interval;
        result1 = result.stream()
                .filter(r->(((r.getTravelClass().getName().equalsIgnoreCase("Business Class"))&&
                        (Period.between(LocalDate.now(),r.getDepartureDate()).getDays()<=28))||
                        ((r.getTravelClass().getName().equalsIgnoreCase("First Class"))&&
                                (Period.between(LocalDate.now(),r.getDepartureDate()).getDays()<=10))||
                        (r.getTravelClass().getName().equalsIgnoreCase("Economy Class"))))
                .collect(Collectors.toList());

            return result1;

    }
}


