package org.FlightBooking.Repository;

import org.FlightBooking.Model.Flight;
import org.FlightBooking.Model.TravelClass;
import org.FlightBooking.Model.TripDetails;
import org.springframework.stereotype.Repository;

import java.text.ParseException;
import java.time.LocalDate;
import java.util.ArrayList;

@Repository
public class TripDetailsRepository {

    private ArrayList tripRepository = new ArrayList();
    private int noOfSeats;

    public TripDetailsRepository() throws ParseException {

        Flight F1 = new Flight(1, "Boeing777", "Indigo");
        Flight F2 = new Flight(2, "AirBusA319", "JetBlue");
        Flight F3 = new Flight(3, "AirBusA321", "Air India");
        Flight F4 = new Flight(4, "Boeing777", "Air India");
        Flight F5 = new Flight(5, "AirBusA319", "JetBlue");
        Flight F6 = new Flight(6, "AirBusA319", "Indigo");

        TravelClass tr1 = new TravelClass(1,"Economy Class",195,1500.00,130);
        TravelClass tr2 = new TravelClass(2,"Business Class",35,3000.00,15);


        TripDetails T1 = new TripDetails(1, "Hyderabad", "Delhi", F1, LocalDate.parse("2019-08-30"),tr1);
        TripDetails T2 = new TripDetails(2,"Hyderabad","Delhi",F1,LocalDate.parse("2019-08-30"),tr2);
        TripDetails T3 = new TripDetails(3,"Hyderabad","Delhi",F1,LocalDate.parse("2019-08-30"),new TravelClass(3,"First Class",8,6000.00,5));
        TripDetails T4 = new TripDetails(4, "Delhi", "Hyderabad", F2, LocalDate.parse("2019-08-25"),new TravelClass(4,"Economy Class",144,1200.00,120));
        TripDetails T5 = new TripDetails(5, "Banglore", "Mumbai", F3, LocalDate.parse("2019-09-02"),new TravelClass(5,"Economy Class",152,2000.00,10));
        TripDetails T6 = new TripDetails(6,"Banglore","Mumbai",F3,LocalDate.parse("2019-09-02"),new TravelClass(6,"Business Class",20,4000.00,10));
        TripDetails T7 = new TripDetails(7, "Mumbai", "Banglore", F6, LocalDate.parse("2019-09-04"),new TravelClass(7,"Economy Class",144,1800.00,120));
        TripDetails T8 = new TripDetails(8, "Tirupathi", "Hyderabad", F5, LocalDate.parse("2019-09-03"),new TravelClass(8,"Economy Class",144,1000.00,100));
        TripDetails T9 = new TripDetails(9, "Hyderabad", "Tirupathi", F4, LocalDate.parse("2019-09-04"),new TravelClass(9,"Economy Class",195,2000.00,120));
        TripDetails T10 = new TripDetails(10,"Hyderabad","Tirupathi",F4,LocalDate.parse("2019-09-04"),new TravelClass(10,"Business Class",35,3000.00,20));
        TripDetails T11 = new TripDetails(11,"Hyderabad","Tirupathi",F4,LocalDate.parse("2019-09-04"),new TravelClass(11,"First Class",8,4500.00,8));
        TripDetails T12 = new TripDetails(12,"Hyderabad","Delhi",F1,LocalDate.parse("2019-09-11"),new TravelClass(12,"First Class",8,6000.00,5));
        TripDetails T13 = new TripDetails(13,"Hyderabad","Delhi",F1,LocalDate.parse("2019-09-03"),new TravelClass(13,"Economy Class",195,3000.00,20));
        TripDetails T14 = new TripDetails(14,"Hyderabad","Delhi",F1,LocalDate.parse("2019-09-03"),new TravelClass(14,"Business Class",30,4000.00,20));
        TripDetails T15 = new TripDetails(15,"Hyderabad","Delhi",F1,LocalDate.parse("2019-08-30"),new TravelClass(15,"First Class",8,6000.00,5));
        TripDetails T16 = new TripDetails(16,"Hyderabad","Delhi",F1,LocalDate.parse("2019-09-11"),new TravelClass(16,"Economy Class",195,2000.00,15));
        TripDetails T17 = new TripDetails(17,"Hyderabad","Delhi",F1,LocalDate.parse("2019-09-11"),new TravelClass(3,"Business Class",30,4000.00,30));



        tripRepository.add(T1);
        tripRepository.add(T2);
        tripRepository.add(T3);
        tripRepository.add(T4);
        tripRepository.add(T4);
        tripRepository.add(T5);
        tripRepository.add(T6);
        tripRepository.add(T7);
        tripRepository.add(T8);
        tripRepository.add(T9);
        tripRepository.add(T10);
        tripRepository.add(T11);
        tripRepository.add(T12);
        tripRepository.add(T13);
        tripRepository.add(T14);
        tripRepository.add(T15);
        tripRepository.add(T16);


    }

    public ArrayList getTripRepository() {
        return tripRepository;
    }
}

