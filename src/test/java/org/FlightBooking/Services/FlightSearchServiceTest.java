package org.FlightBooking.Services;


import org.FlightBooking.Model.Flight;
import org.FlightBooking.Model.TravelClass;
import org.FlightBooking.Model.TripDetails;
import org.FlightBooking.Repository.TripDetailsRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class FlightSearchServiceTest {

    @Mock
    private TripDetailsRepository tripDetailsRepository;
    private ArrayList<TripDetails> tripDetails = new ArrayList<>();

    public  FlightSearchServiceTest() {

        Flight f1 = new Flight(4, "AIRBUS321", "Go Air");
        TravelClass test1 = new TravelClass(1,"Economy Class",195,3000.00,16);

        TripDetails test2 = new TripDetails(1, "hyd", "delhi", f1,LocalDate.parse("2019-08-30"),test1);
        TripDetails test3 = new TripDetails(2,"hyd","delhi",f1,LocalDate.parse("2019-08-30"),new TravelClass(2,"Business Class",35,4000.00,20));
        TripDetails test4 = new TripDetails(3,"hyd","delhi",f1,LocalDate.parse("2019-08-30"),new TravelClass(3,"First Class",8,6000.00,6));
        TripDetails test5 = new TripDetails(4,"hyd","delhi",f1,LocalDate.parse("2019-09-20"),new TravelClass(4,"First Class",8,4000.00,8));
        TripDetails test6 = new TripDetails(5,"hyd","delhi",f1,LocalDate.parse("2019-09-15"),new TravelClass(5,"Business Class",35,3000.00,35));
        TripDetails test7 = new TripDetails(5,"hyd","delhi",f1,LocalDate.parse("2019-10-12"),new TravelClass(5,"Business Class",35,3000.00,35));

        tripDetails.add(test2);
        tripDetails.add(test3);
        tripDetails.add(test4);
        tripDetails.add(test5);
        tripDetails.add(test6);

    }

    @Test
    public void shouldReturnAllFlightsOnADayInAClass(){

        FlightSearchService flightSearchService = new FlightSearchService(tripDetailsRepository);

        when(tripDetailsRepository.getTripRepository()).thenReturn(tripDetails);


        List<TripDetails> flightsInAClassOnADay = flightSearchService.findBySourceDestinationOnADayInAClass("hyd","delhi",3,"Economy Class",LocalDate.parse("2019-08-30"));
        assertEquals(1,flightsInAClassOnADay.size());


    }

    @Test
    public void shouldReturnAllFlightsInAClass(){
        FlightSearchService flightSearchService = new FlightSearchService(tripDetailsRepository);

        when(tripDetailsRepository.getTripRepository()).thenReturn(tripDetails);


        List<TripDetails> flightsInAClass = flightSearchService.findBySourceDestinationOnADayInAClass("hyd","delhi",3,"Economy Class",null);
        assertEquals(1,flightsInAClass.size());
    }
    @Test
    public void returnBookingAmountEconomy(){
        FlightSearchService flightSearchService = new FlightSearchService(tripDetailsRepository);
        FareCalculateService fareCalculateService = new FareCalculateService(tripDetailsRepository);

        when(tripDetailsRepository.getTripRepository()).thenReturn(tripDetails);


        List<TripDetails> resultAfterSearch = flightSearchService.findBySourceDestinationOnADayInAClass("hyd","delhi",3,"Economy Class",null);

        List<TripDetails> flightsInAClassForBookingAmount = fareCalculateService.fareCalculateServiceForEachClass(resultAfterSearch,3);
        assertEquals(14400.00,flightsInAClassForBookingAmount.get(0).getBookingAmount(),0.0001);
    }
    @Test
    public void returnBookingAmountBusiness(){
        FlightSearchService flightSearchService = new FlightSearchService(tripDetailsRepository);
        FareCalculateService fareCalculateService = new FareCalculateService(tripDetailsRepository);

        when(tripDetailsRepository.getTripRepository()).thenReturn(tripDetails);

        List<TripDetails> resultAfterSearch = flightSearchService.findBySourceDestinationOnADayInAClass("hyd","delhi",3,"Business Class",LocalDate.parse("2019-08-30"));

        List<TripDetails> flightsInBusinessClassForBookingAmount = fareCalculateService.fareCalculateServiceForEachClass(resultAfterSearch,3);
        assertEquals(16800.00,flightsInBusinessClassForBookingAmount.get(0).getBookingAmount(),0.0001);
    }
    @Test
    public void returnBookingAmountFirst(){
        FlightSearchService flightSearchService = new FlightSearchService(tripDetailsRepository);
        FareCalculateService fareCalculateService = new FareCalculateService(tripDetailsRepository);

        when(tripDetailsRepository.getTripRepository()).thenReturn(tripDetails);

        List<TripDetails> resultAfterSearch = flightSearchService.findBySourceDestinationOnADayInAClass("hyd","delhi",3,"First Class",LocalDate.parse("2019-08-30"));

        List<TripDetails> flightsInFirstClassForBookingAmount = fareCalculateService.fareCalculateServiceForEachClass(resultAfterSearch,3);
        assertEquals(36000.00,flightsInFirstClassForBookingAmount.get(0).getBookingAmount(),0.0001);
    }

    @Test
    public void shouldNotReturnFirstClassScheduledFlightAfter10Days(){
        FlightSearchService flightSearchService = new FlightSearchService(tripDetailsRepository);
        FareCalculateService fareCalculateService = new FareCalculateService(tripDetailsRepository);

        when(tripDetailsRepository.getTripRepository()).thenReturn(tripDetails);

        List<TripDetails> resultAfterSearch = flightSearchService.findBySourceDestinationOnADayInAClass("hyd","delhi",3,"First Class",null);

        List<TripDetails> flightsInFirstClassForBookingAmount = fareCalculateService.fareCalculateServiceForEachClass(resultAfterSearch,3);
        assertEquals(1,flightsInFirstClassForBookingAmount.size());
    }
    @Test
    public void shouldNotReturnBusinessFlightScheduledAfter28Days(){
        FlightSearchService flightSearchService = new FlightSearchService(tripDetailsRepository);
        FareCalculateService fareCalculateService = new FareCalculateService(tripDetailsRepository);

        when(tripDetailsRepository.getTripRepository()).thenReturn(tripDetails);

        List<TripDetails> resultAfterSearch = flightSearchService.findBySourceDestinationOnADayInAClass("hyd","delhi",3,"Business Class",null);

        List<TripDetails> flightsInBusinessClassForBookingAmount = fareCalculateService.fareCalculateServiceForEachClass(resultAfterSearch,3);
        assertEquals(2,flightsInBusinessClassForBookingAmount.size());
    }



}